package dadosIniciais;
import java.util.ArrayList;
import java.util.Collection;

import dominio.Cidade;
import dominio.Equipe;
import dominio.Divisao;


public class DadosBRA {

	private static Collection<Cidade> cidades;
	
	private static Collection<Equipe> equipes;
	
	static{

		cidades = new ArrayList<Cidade>();
		
		equipes = new ArrayList<Equipe>();
		
		Cidade recife = new Cidade(0, "Recife",-8.1179,-34.9370);
		Cidade portoAlegre = new Cidade(1, "Porto Alegre",-30.1022,-51.1559);
		Cidade saoPaulo = new Cidade(2, "S�o Paulo",-23.6824,-46.5952);
		Cidade chapeco = new Cidade(3, "Chapec�",-27.7583,-50.7122);
		Cidade santos = new Cidade(4, "Santos",-23.9549,-46.3449);
		Cidade rioDeJaneiro = new Cidade(5, "Rio de Janeiro",-22.9112,-43.4483);
		Cidade curitiba = new Cidade(6, "Curitiba",-25.4951,-49.2874);
		Cidade salvador = new Cidade(7, "Salvador",-12.8809,-38.4175);
		Cidade beloHorizonte = new Cidade(8, "Belo Horizonte",-19.9178,-43.9603);
		Cidade campinas = new Cidade(9, "Campinas",-22.8951,-47.0302);
		Cidade florianopolis = new Cidade(10, "Florian�polis",-27.6142,-48.4828);
		
		cidades.add(recife);
		cidades.add(portoAlegre);
		cidades.add(saoPaulo);
		cidades.add(chapeco);
		cidades.add(santos);
		cidades.add(rioDeJaneiro);
		cidades.add(curitiba);
		cidades.add(salvador);
		cidades.add(beloHorizonte);
		cidades.add(campinas);
		cidades.add(florianopolis);
		
		
		equipes.add(new Equipe("Santa Cruz",recife, 1));
		equipes.add(new Equipe("Sport",recife, 2));
		equipes.add(new Equipe("Gr�mio",portoAlegre, 3));
		equipes.add(new Equipe("Internacional",portoAlegre, 4));
		equipes.add(new Equipe("Corinthians",saoPaulo, 5));
		equipes.add(new Equipe("S�o Paulo",saoPaulo, 6));
		equipes.add(new Equipe("Palmeiras",saoPaulo, 7));
		equipes.add(new Equipe("Chapecoense",chapeco, 8));
		equipes.add(new Equipe("Santos",santos, 9));
		equipes.add(new Equipe("Botafogo",rioDeJaneiro, 10));
		equipes.add(new Equipe("Flamengo",rioDeJaneiro, 11));
		equipes.add(new Equipe("Fluminense",rioDeJaneiro, 12));
		equipes.add(new Equipe("Atl�tico/PR",curitiba, 13));
		equipes.add(new Equipe("Coritiba",curitiba, 14));
		equipes.add(new Equipe("Vit�ria",salvador, 15));
		equipes.add(new Equipe("Am�rica/MG",beloHorizonte, 16));
		equipes.add(new Equipe("Atl�tico/MG",beloHorizonte, 17));
		equipes.add(new Equipe("Cruzeiro",beloHorizonte, 18));
		equipes.add(new Equipe("Ponte Preta",campinas, 19));
		equipes.add(new Equipe("Figueirense",florianopolis, 20));
		
		
	}

	public static Collection<Equipe> getEquipes() {
		return equipes;
	}
	

	public static Collection<Cidade> getCidades() {
		return cidades;
	}
	
}
