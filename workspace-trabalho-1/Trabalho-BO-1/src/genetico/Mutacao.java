package genetico;

import java.util.Collection;

import utils.RandomUtils;
import utils.RoletaEquipe;
import dadosIniciais.DadosBRA;
import dadosIniciais.DadosNBA;
import dadosIniciais.Parametros;
import dominio.Calendario;
import dominio.Equipe;
import dominio.Partida;

public class Mutacao {

	/**
	 * Realiza uma muta��o no calend�rio
	 * @param calendario
	 */
	public static void mutacao(Calendario calendario){
		
		int x = RandomUtils.getNumeroInteiroAleatorioEntre(1, Parametros.PESO_MUTACAO_TROCA_EQUIPE + Parametros.PESO_MUTACAO_TROCA_RODADA);
		
		if (x <= Parametros.PESO_MUTACAO_TROCA_EQUIPE)
			mutacaoTrocaEquipes(calendario);
		else
			mutacaoTrocaRodadas(calendario);
		
	}
	
	/**
	 * Efetua uma muta��o no calend�rio
	 * Escolhe duas equipes da mesma divis�o aleatoriamente e troca a posi��o dessas duas equipes em todas as rodadas
	 * @param calendario
	 */
	private static void mutacaoTrocaEquipes(Calendario calendario){
		
		//Escolhe duas equipes aleatoriamente
		RoletaEquipe roleta = new RoletaEquipe(DadosBRA.getEquipes());
		Equipe equipe1 = roleta.getNextEquipe();
		Equipe equipe2 = roleta.getNextEquipe();
		
		//Trocar as duas equipes no calend�rio
		for (int i = 0; i < calendario.getCalendario().length; i++)
			for (int j = 0; j < calendario.getCalendario()[0].length; j++){
				Partida p = calendario.getCalendario()[i][j];
				if (p != null){
					if (p.getMandante().getId() == equipe1.getId())
						p.setMandante(equipe2);
					else if (p.getMandante().getId() == equipe2.getId())
						p.setMandante(equipe1);
					
				if (p.getAdversario().getId() == equipe1.getId())
					p.setAdversario(equipe2);
				else if (p.getAdversario().getId() == equipe2.getId())
					p.setAdversario(equipe1);
				}
			}
		
	}
	
	/**
	 * Efetua uma muta��o no calend�rio
	 * Escolhe duas rodadas aleatoriamente e as troca de lugar no calend�rio
	 * @param calendario
	 */
	private static void mutacaoTrocaRodadas(Calendario calendario){
		
		//Determina as duas rodadasque ser�o trocadas
		int rodada1 = RandomUtils.getNumeroInteiroAleatorioEntre(0, (Parametros.NUMERO_MAXIMO_RODADAS/2) -1);
		int rodada2 = 0;
		do{
			rodada2 = RandomUtils.getNumeroInteiroAleatorioEntre(0, (Parametros.NUMERO_MAXIMO_RODADAS/2) -1);
		} while(rodada2 == rodada1);
			
		
		//Trocar as rodadas
		Partida[][] matriz = calendario.getCalendario();
		Partida[] aux = matriz[rodada1];
		matriz[rodada1] = matriz[rodada2];
		matriz[rodada2] = aux;
		
		//Trocar as rodadas do returno
		rodada1 = rodada1 + 19;
		rodada2 = rodada2 + 19;
		aux = matriz[rodada1];
		matriz[rodada1] = matriz[rodada2];
		matriz[rodada2] = aux;
		
		calendario.setCalendario(matriz);
	}
	
}
