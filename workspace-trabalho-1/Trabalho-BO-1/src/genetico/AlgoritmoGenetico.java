package genetico;

import java.util.ArrayList;
import java.util.Collection;

import utils.ArrayUtils;
import utils.CalendarioUtils;
import utils.CalendarioUtilsBRA;
import dadosIniciais.Parametros;
import dominio.Calendario;

public class AlgoritmoGenetico {

	//Tempos em que sistema ir� exibir o relat�rio
	public static int[] TEMPOS_RELATORIO = new int[]{1,10,100,1000,10000,100000,1000000,2000000,3000000,4000000,5000000,6000000,7000000,8000000,9000000,10000000};
	
	/**
	 * Obtem o melhor calend�rio utilizando o algoritmo gen�tico
	 * @return
	 */
	public static Calendario obterMelhorCalendario(){
		
		Calendario melhor = null;
		
		//Iniciar a popula��o
		Collection<Calendario> populacao = new ArrayList<Calendario>();
		for (int i = 0; i < Parametros.POPULACAO; i++){
			Calendario cal = CalendarioUtilsBRA.getCalendarioAleatorio();
			populacao.add(cal);
			System.out.println("Fitness inicial: " + Fitness.calcularFitness(cal));
		}
		
		Long inicio = System.currentTimeMillis();
		
		//Repeti��o principal do la�o
		for (int i = 0; i < Parametros.NUMERO_LACOS_ALGORITMO_GENETICO; i++){
			
			//Calcular melhor calendario
			for (Calendario cal : populacao){
				if (cal.getFitness() == 0)
					cal.setFitness(Fitness.calcularFitness(cal));
				if (melhor == null || melhor.getFitness() > cal.getFitness())
					melhor = cal;
			}
			
			//Exibir relat�rio parcial
			if (ArrayUtils.inArray(i, TEMPOS_RELATORIO) || i == Parametros.NUMERO_LACOS_ALGORITMO_GENETICO-1){
				System.out.println("Ciclo: "+i);
				System.out.println("Fitness do melhor: "+melhor.getFitness());
				System.out.println("Tempo de execu��o: "+((System.currentTimeMillis()-inicio)/1000)+"s");
				System.out.println("");
			}
			
			Collection<Calendario> populacao2 = new ArrayList<Calendario>(); 
			
			//Gerar descendentes e modifica-los
			for (int c = 0; c < Parametros.POPULACAO/2; c++){
				
				Calendario pai1 = SelecaoTorneio.selecionarPorTorneio(populacao, null);
				Calendario pai2 = SelecaoTorneio.selecionarPorTorneio(populacao, pai1);
				
				Calendario filho1 = pai1.getCopia();
				Calendario filho2 = pai2.getCopia();
				
				/** Modificar filho1 at� que muta��o seja v�lida */
				Calendario mutacaoFilho1 = null;
				do{
					mutacaoFilho1 = filho1.getCopia();
					Mutacao.mutacao(mutacaoFilho1);
				}
				while (!CalendarioUtilsBRA.isValido(mutacaoFilho1));
				
				/** Modificar filho2 at� que muta��o seja v�lida */
				Calendario mutacaoFilho2 = null;
				do{
					mutacaoFilho2 = filho2.getCopia();
					Mutacao.mutacao(mutacaoFilho2);
				}
				while (!CalendarioUtilsBRA.isValido(mutacaoFilho2));
				
				
				populacao2.add(mutacaoFilho1);
				populacao2.add(mutacaoFilho2);
				
			}
			
			populacao = populacao2;
			
		}
		
		return melhor;
		
	}
	
}
