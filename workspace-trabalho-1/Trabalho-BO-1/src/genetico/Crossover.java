package genetico;

import utils.ArrayUtils;
import dadosIniciais.DadosBRA;
import dadosIniciais.Parametros;
import dominio.Calendario;
import dominio.Partida;

public class Crossover {

	/**
	 * O cruzamento consiste em trocar as 1�s rodadas, e atualizar os restantes das rodadas trocando os times correspondentes
	 * Crossover faz cruzamento de calendario1 para calendario2. M�todo dever� ser chamado novamente invertendo os argumentos para gerar 2� filho
	 * @param calendario1
	 * @param calendario2
	 * @param filho1
	 * @param filho2
	 */
	public static Calendario crossover(Calendario calendario1, Calendario calendario2){
		
		Partida[][] matrizFilho = new Partida[Parametros.NUMERO_MAXIMO_RODADAS][15];
		
		//Montar correspond�ncias
		for (int i = 0; i < DadosBRA.getEquipes().size()/2; i++){
			calendario1.getCalendario()[0][i].getMandante().setCorrespondente(
					calendario2.getCalendario()[0][i].getMandante()	
					);
			calendario1.getCalendario()[0][i].getAdversario().setCorrespondente(
					calendario2.getCalendario()[0][i].getAdversario()	
					);
		}
		
		//Trocar times com base na correnpod�ncia entre os calend�rios
		for (int i = 0; i < Parametros.NUMERO_MAXIMO_RODADAS; i++)
			for (int j = 0; j < DadosBRA.getEquipes().size()/2; j++){
				matrizFilho[i][j] = new Partida(
					calendario1.getCalendario()[i][j].getMandante().getCorrespondente(),
					calendario1.getCalendario()[i][j].getAdversario().getCorrespondente()
				);
			}
		
		Calendario filho = new Calendario();
		filho.setCalendario(matrizFilho);

		return filho;
		
	}
	
}
