package hillClimbing;

import utils.ArrayUtils;
import utils.CalendarioUtilsBRA;
import dadosIniciais.Parametros;
import dominio.Calendario;

public class AlgoritmoSteepestAscentHillClimbing {

	//Tempos em que sistema ir� exibir o relat�rio
	public static int[] TEMPOS_RELATORIO = new int[]{1,10,100,1000,10000,100000,1000000,2000000,3000000,4000000,5000000,6000000,7000000,8000000,9000000,10000000};
	
	/**
	 * Obtem o melhor calend�rio utilizando o algoritmo gen�tico
	 * @return
	 */
	public static Calendario obterMelhorCalendario(){
		
		Calendario atual = CalendarioUtilsBRA.getCalendarioAleatorio();
		atual.setFitness(Quality.calcularQuality(atual));
		Calendario melhor = null;
		
		Long inicio = System.currentTimeMillis();
		
		//Repeti��o principal do la�o
		for (int i = 0; i < Parametros.NUMERO_LACOS_ALGORITMO_HILLCLIMBING; i++){
			
			//Calcular melhor calendario
			if (melhor == null || melhor.getFitness() > atual.getFitness())
				melhor = atual;
			
			
			//Exibir relat�rio parcial
			if (ArrayUtils.inArray(i, TEMPOS_RELATORIO) || i == Parametros.NUMERO_LACOS_ALGORITMO_HILLCLIMBING-1){
				System.out.println("Ciclo: "+i);
				System.out.println("Fitness do melhor: "+melhor.getFitness());
				System.out.println("Tempo de execu��o: "+((System.currentTimeMillis()-inicio)/1000)+"s");
				System.out.println("");
			}
			
			Calendario melhorVizinho = null;
			
			for (int j = 0; j < Parametros.NUMERO_VIZINHOS_HILLCLIMBING; j++){
				do{
					atual = melhor.getCopia();
					Tweak.tweak(atual);
				} while (!CalendarioUtilsBRA.isValido(atual));
				
				atual.setFitness(Quality.calcularQuality(atual));
				
				if (melhorVizinho == null || melhorVizinho.getFitness() > atual.getFitness())
					melhorVizinho = atual;
				
			}
			
			atual = melhorVizinho;
			
		}
		
		return melhor;
		
	}
	
}
