package dominio;

import java.util.ArrayList;
import java.util.Collection;

public class Equipe {

	private String nome;
	private Cidade cidade;
	private Divisao divisao;
	private char conferencia; //'L' ou 'O' 
	private int id;
	
	//Partidas da equipe. Elemento transiente.
	private Collection<Partida> partidas;
	
	//Equipe correspondente. Elemento transiente usado no crossover
	private Equipe correspondente;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Equipe(String nome, Cidade cidade, Divisao divisao, char conferencia, int id){
		this.nome = nome;
		this.cidade = cidade;
		this.divisao= divisao;
		this.conferencia = conferencia;
		this.id = id;
		
		partidas = new ArrayList<Partida>();
	}
	
	public Equipe(String nome, Cidade cidade, int id){
		this.nome = nome;
		this.cidade = cidade;
		this.id = id;
		
		partidas = new ArrayList<Partida>();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Divisao getDivisao() {
		return divisao;
	}

	public void setDivisao(Divisao divisao) {
		this.divisao = divisao;
	}

	public char getConferencia() {
		return conferencia;
	}

	public void setConferencia(char conferencia) {
		this.conferencia = conferencia;
	}

	public Equipe getCorrespondente() {
		return correspondente;
	}

	public void setCorrespondente(Equipe correspondente) {
		this.correspondente = correspondente;
	}
	
}
