package dominio;

import dadosIniciais.DadosBRA;
import dadosIniciais.Parametros;

public class Calendario {

	private Partida[][] calendario;
	
	//Fitness de um calend�rio
	private int fitness;
	
	public Calendario(){
		calendario = new Partida[Parametros.NUMERO_MAXIMO_RODADAS][DadosBRA.getEquipes().size()/2];
	}

	public Partida[][] getCalendario() {
		return calendario;
	}

	public void setCalendario(Partida[][] calendario) {
		this.calendario = calendario;
	}
	
	public int getFitness() {
		return fitness;
	}

	public void setFitness(int fitness) {
		this.fitness = fitness;
	}

	/**
	 * Retorna se h� alguma rodada incompleta
	 * @param calendario
	 * @return
	 */
	public boolean temRodadaIncompleta(){
		
		for (int i = 0; i < 82; i++)
			for (int j = 0; j < 15; j++)
				if (this.getCalendario()[i][j] == null)
					return true;
		return false;
		
	}
	
	/**
	 * Retorna a �ltima partida de um calend�rio
	 * @param calendario
	 * @return
	 */
	public Partida getUltimaPartida(){
		
		for (int i = Parametros.NUMERO_MAXIMO_RODADAS-1; i >= 0; i--)
			for (int j = 14; j >= 0; j--)
				if (this.getCalendario()[i][j] != null)
					return this.getCalendario()[i][j];
		return null;
		
	}
	
	/**
	 * Exclui a �ltima partida de um calend�rio
	 * @param calendario
	 * @return
	 */
	public void limparUltimaPartida(){
		
		for (int i = Parametros.NUMERO_MAXIMO_RODADAS-1; i >= 0; i--)
			for (int j = 14; j >= 0; j--)
				if (this.getCalendario()[i][j] != null){
					this.getCalendario()[i][j] = null;
					return;
				}
		
	}
	
	public String toString(){
		
		String rodada = "";
		
		for (int i = 0; i < Parametros.NUMERO_MAXIMO_RODADAS; i++){
			System.out.println((i+1)+"� Rodada");
			for (int j = 0; j < DadosBRA.getEquipes().size()/2; j++)
				System.out.println(getCalendario()[i][j].getMandante().getNome() + " X " +getCalendario()[i][j].getAdversario().getNome());
			
			System.out.println("");
		}
		
		return rodada;
		
	}
	
	public Calendario getCopia(){
		
		Partida[][] matrizCopia = new Partida[Parametros.NUMERO_MAXIMO_RODADAS][15];
		
		for (int i = 0; i < Parametros.NUMERO_MAXIMO_RODADAS; i++){
			for (int j = 0; j < DadosBRA.getEquipes().size()/2; j++)
				matrizCopia[i][j] = getCalendario()[i][j].getCopia();
		}
		
		Calendario copia = new Calendario();
		copia.setCalendario(matrizCopia);

		return copia;
		
	}
}
