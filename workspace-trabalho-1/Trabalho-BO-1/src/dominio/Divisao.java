package dominio;

public enum Divisao {

	/**
	 * Conferência Leste
	 */
	ATLANTICO,
	CENTRAL,
	SUDESTE,
	
	/**
	 * Conferência Oeste
	 */
	NOROESTE,
	PACIFICO,
	SUDOESTE
}
