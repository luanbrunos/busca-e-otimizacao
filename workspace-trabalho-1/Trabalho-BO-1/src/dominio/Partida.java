package dominio;

public class Partida {

	private Equipe mandante;
	private Equipe adversario;
	
	public Partida(Equipe mandante, Equipe adversario){
		this.mandante = mandante;
		this.adversario = adversario;
	}

	public Equipe getMandante() {
		return mandante;
	}

	public void setMandante(Equipe mandante) {
		this.mandante = mandante;
	}

	public Equipe getAdversario() {
		return adversario;
	}

	public void setAdversario(Equipe adversario) {
		this.adversario = adversario;
	}
	
	public String toString(){
		return mandante.getNome() + " X " + adversario.getNome();
	}
	
	/**
	 * Retorna uma c�pia da partida
	 * @return
	 */
	public Partida getCopia(){
		return new Partida(getMandante(),getAdversario());		
	}
	
	/**
	 * Retorna uma c�pia da partida invertendo mandante e advers�rio
	 * @return
	 */
	public Partida getCopiaInvertida(){
		return new Partida(getAdversario(), getMandante());		
	}
}
