package utils;

import java.util.ArrayList;
import java.util.Collection;

import dominio.Calendario;
import dominio.Cidade;
import dominio.Equipe;
import dominio.Partida;

public class ArrayUtils {

	/**
	 * Converte uma List de Cidade em um Array
	 * @param cidades
	 * @return
	 */
	public static Cidade[] toArrayCidade(Collection<Cidade> cidades){
		
		Object[] cidadesObj = cidades.toArray();
		Cidade[] cidadesArray = new Cidade[cidadesObj.length];
		for (int i = 0; i < cidadesObj.length; i++)
			cidadesArray[i] = (Cidade)cidadesObj[i];
		
		return cidadesArray;
		
	}
	
	/**
	 * Converte uma List de Equipe em um Array
	 * @param equipes
	 * @return
	 */
	public static Equipe[] toArrayEquipe(Collection<Equipe> equipes){
		
		Object[] equipesObj = equipes.toArray();
		Equipe[] equipesArray = new Equipe[equipesObj.length];
		for (int i = 0; i < equipesObj.length; i++)
			equipesArray[i] = (Equipe)equipesObj[i];
		
		return equipesArray;
		
	}
	
	/**
	 * Converte uma List de Calendario em um Array
	 * @param cidades
	 * @return
	 */
	public static Calendario[] toArrayCalendario(Collection<Calendario> calendarios){
		
		Object[] calendariosObj = calendarios.toArray();
		Calendario[] calendariosArray = new Calendario[calendariosObj.length];
		for (int i = 0; i < calendariosObj.length; i++)
			calendariosArray[i] = (Calendario)calendariosObj[i];
		
		return calendariosArray;
		
	}
	
	/**
	 * Converte uma Array de Equipe em um List
	 * @param equipes
	 * @return
	 */
	public static Collection<Equipe> toCollectionEquipe(Equipe[] equipes){
		
		Collection<Equipe> equipesCol = new ArrayList<Equipe>();
		for (int i = 0; i < equipes.length; i++)
			equipesCol.add(equipes[i]);
		
		return equipesCol;
		
	}
	
	/**
	 * Retorna uma copia do Array
	 * @param equipes
	 * @return
	 */
	public static Equipe[] copia(Equipe[] equipes){
		Equipe[] retorno = new Equipe[equipes.length];
		for (int i = 0; i < equipes.length; i++)
			retorno[i] = equipes[i];
		return retorno;
	}
	
	/**
	 * Retorna uma copia do Array
	 * @param calendarios
	 * @return
	 */
	public static Calendario[] copia(Calendario[] calendarios){
		Calendario[] retorno = new Calendario[calendarios.length];
		for (int i = 0; i < calendarios.length; i++)
			retorno[i] = calendarios[i];
		return retorno;
	}
	
	/**
	 * Retira o elemento equipe do array equipes e retorna uma C�PIA
	 */
	public static Equipe[] retirarElemento(Equipe[] equipes, Equipe equipe){
		
		Collection<Equipe> equipeCol = toCollectionEquipe(equipes);
		equipeCol.remove(equipe);
		return toArrayEquipe(equipeCol);
		
	}
	
	/**
	 * Gira um vetor de equipes, de modo a deixar todos os elementos na posi��o subsequente, e o �ltimo elemento na primeira posi��o
	 * @param equipes
	 * @return
	 */
	public static Equipe[] girarEquipes(Equipe[] equipes){
		
		Equipe primeiro = equipes[0];
				
		for (int i = 0; i < equipes.length-1; i++)
			equipes[i] = equipes[i+1];
		
		equipes[equipes.length-1] = primeiro;
		return equipes;
		
	}
	
	/**
	 * mistura as posi��es do vetor
	 * @param equipes
	 * @return
	 */
	public static Equipe[] misturarEquipes(Equipe[] equipes){
		
		RoletaEquipe roleta = new RoletaEquipe(equipes);
		Collection<Equipe> aux = new ArrayList<Equipe>();
		while (roleta.hasMoreEquipes())
			aux.add(roleta.getNextEquipe());
		return ArrayUtils.toArrayEquipe(aux);
		
	} 
	
	/**
	 * Copia uma partida
	 * @param partida
	 * @return
	 */
	public static Partida[] copiaPartidas(Partida[] partida){
		
		Partida[] copia = new Partida[partida.length];
		for (int i = 0; i < partida.length; i++){
			if (partida[i] == null)
				continue;
			copia[i] = new Partida(partida[i].getMandante(),partida[i].getAdversario());
		}
		return copia;
		
	}
	
	/**
	 * Retorna se um int x est� num array
	 * @param x
	 * @param array
	 * @return
	 */
	public static boolean inArray(int x, int[] array){
		
		for (int i = 0; i < array.length; i++ )
			if (array[i] == x)
				return true;
		
		return false;
	}
}
