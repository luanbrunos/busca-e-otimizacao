package utils;

import java.util.Random;

/**
 * Classe de m�todos utilit�rios para retorno de n�meros aleat�rios
 * 
 * @author Luan
 *
 */
public class RandomUtils {

	/**
	 * Retorna um n�mero inteiro aleat�rio entre comeco e fim
	 * @param comeco
	 * @param fim
	 * @return
	 */
	public static int getNumeroInteiroAleatorioEntre(int comeco, int fim){

		Random r = new Random();
		return comeco + r.nextInt(fim+1);
		
	}
	
	/**
	 * Retorna um n�mero inteiro aleat�rio entre 0 e maximo
	 * @param comeco
	 * @param fim
	 * @return
	 */
	public static int getNumeroPositivoAleatorioAte(int maximo){

		return getNumeroInteiroAleatorioEntre(0,maximo);
		
	}
	
}

