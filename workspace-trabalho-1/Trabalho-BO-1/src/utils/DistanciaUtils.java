package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import dadosIniciais.Parametros;
import dominio.Cidade;
import dominio.Equipe;
import dominio.Partida;
import dominio.Calendario;
import utils.MatrizDistancias;

public class DistanciaUtils {

	/**
	 * Calcula a dist�ncia entre duas cidades usando a API do google
	 * @param cidadeOrigem
	 * @param cidadeDestino
	 * @return
	 */
	public static int distanciaEntreGoogle(Cidade cidadeOrigem, Cidade cidadeDestino){
		
		String coordenadasOrigem = cidadeOrigem.getLatitude()+","+cidadeOrigem.getLongitude();
		String coordenadasDestino = cidadeDestino.getLatitude()+","+cidadeDestino.getLongitude();
		
		try {
			URL url = new URL("https://maps.googleapis.com/maps/api/distancematrix/xml?origins="+coordenadasOrigem+"&destinations="+coordenadasDestino+"8&key="+Parametros.CHAVE_GOOGLE_API);
			URLConnection conn = url.openConnection();
			BufferedReader buffer = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String linha;
			
			while ((linha = buffer.readLine()) != null){
				
				/*if (linha.contains("<distance>")){
					//Ent�o a linha que cont�m a dist�ncia � a pr�xima
					linha = buffer.readLine(); //--> linha que cont�m dist�ncia
					int indexInicio = linha.indexOf("<value>") + 7;
					int indexFim = linha.indexOf("</value>");
					return Integer.parseInt(linha.substring(indexInicio, indexFim));
				}*/
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		return 0;
	}
	
	/**
	 * Calcula as dist�ncias entre duas cidades em linha reta
	 * @param cidadeOrigem
	 * @param cidadeDestino
	 * @return
	 */
	public static int distanciaEntre(Cidade cidadeOrigem, Cidade cidadeDestino){
				
		return (int) GeoUtils.geoDistanceInKm(cidadeOrigem.getLatitude(), cidadeOrigem.getLongitude(), cidadeDestino.getLatitude(), cidadeDestino.getLongitude()) * 1000;
		
	}
	
	/**
	 * Calcula a distancia total percorrida pelas equipes em um calendario
	 */
	public static int distanciaTotalCalendario(Calendario calendario, MatrizDistancias matriz, Equipe[] equipes){
		
		int distancias[] = new int[30];
		int distanciaTotal = 0, mandanteId, adversarioId;
		Partida[] rodadaPassada = new Partida[15];
		
		for(int i=0; i< Parametros.NUMERO_MAXIMO_RODADAS; i++){
			
			 for (int j=0; j<15; j++){
				 
				 adversarioId = calendario.getCalendario()[i][j].getAdversario().getId();
				 mandanteId = calendario.getCalendario()[i][j].getMandante().getId();
				 
				 if(i==0){
					 
					distancias[adversarioId] = matriz.getDistanciaEntreCidades(
							calendario.getCalendario()[i][j].getAdversario().getCidade(), 
							calendario.getCalendario()[i][j].getMandante().getCidade());
					
				 }else{
					 //se a equipe adversaria jogou fora na rodada passada
					 if (rodadaPassada[adversarioId].getAdversario().getId() == adversarioId){
						 distancias[adversarioId] = distancias[adversarioId] + matriz.getDistanciaEntreCidades(
								 rodadaPassada[adversarioId].getMandante().getCidade(),
								 calendario.getCalendario()[i][j].getMandante().getCidade());
					 }else{
						 distancias[adversarioId] = distancias[adversarioId]+ matriz.getDistanciaEntreCidades(
								 rodadaPassada[adversarioId].getMandante().getCidade(),
								 calendario.getCalendario()[i][j].getAdversario().getCidade());
					 }
					 //se a equipe mandante jogou fora na rodada passada
					 if (rodadaPassada[mandanteId].getAdversario().getId() == mandanteId){
						 distancias[mandanteId] = distancias[mandanteId]+ matriz.getDistanciaEntreCidades(
								 rodadaPassada[mandanteId].getMandante().getCidade(), 
								 calendario.getCalendario()[i][j].getMandante().getCidade());
					 }
				 }
				 // salva a rodada passada
				 rodadaPassada[adversarioId] = calendario.getCalendario()[i][j];
				 rodadaPassada[mandanteId] = calendario.getCalendario()[i][j];
			 }
		}
		
		for (int l = 0; l<30; l++){
			distanciaTotal = distanciaTotal + distancias[l];
		}
		
		return distanciaTotal;
	}
	
}
