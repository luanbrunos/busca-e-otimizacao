package utils;

import java.util.ArrayList;
import java.util.Collection;

import dominio.Calendario;
import dominio.Equipe;

/**
 * Tem inicialmente um conjunto de calendários. Retorna os calendários, um por um, aleatoriamente.
 * Elimina os calendários já sorteadas
 * 
 * @author Luan
 *
 */
public class RoletaCalendario {

	private Calendario[] calendarios; 
	
	public RoletaCalendario(Collection<Calendario> calendarios){
		this.calendarios = ArrayUtils.toArrayCalendario(calendarios);
	}
	
	public RoletaCalendario(Calendario[] calendarios){
		this.calendarios = ArrayUtils.copia(calendarios);
	}
	
	public Calendario getNextEquipe(){
		
		if (!hasMoreCalendarios())
			return null;
		
		int proximaPosicao = RandomUtils.getNumeroInteiroAleatorioEntre(0, calendarios.length-1);
		while(calendarios[proximaPosicao] == null){
			proximaPosicao++;
			if (proximaPosicao == calendarios.length)
				proximaPosicao = 0;
		}
		
		Calendario retorno = calendarios[proximaPosicao];
		calendarios[proximaPosicao] = null;
		return retorno;
		
	}
	
	public boolean hasMoreCalendarios(){
		for (int i = 0; i < calendarios.length; i++)
			if (calendarios[i] != null)
				return true;
		return false;
	}
	
	public Collection<Calendario> getCalendariosNaoSorteados(){
		
		Collection<Calendario> naoSorteados = new ArrayList<Calendario>();
		for (int i = 0; i < calendarios.length; i++)
			if (calendarios[i] != null)
				naoSorteados.add(calendarios[i]);
		return naoSorteados;
		
	}
	
}
