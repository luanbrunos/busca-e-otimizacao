package nba;

import utils.MatrizDistancias;
import dadosIniciais.DadosNBA;
import dadosIniciais.Parametros;
import dominio.Calendario;
import dominio.Cidade;
import dominio.Equipe;
import dominio.Partida;

public class Fitness {

	/**
	 * Calcula o fitness do calend�rio
	 * @param calendario
	 * @return
	 */
	public static int calcularFitness(Calendario calendario){
		
		int distanciaTotal = 0;
		MatrizDistancias matriz = new MatrizDistancias();
		
		for (Equipe equipeAtual : DadosNBA.getEquipes()){
			
			//Equipe come�a da sua pr�pria cidade
			int distanciaEquipe = 0;
			Cidade cidadeAtualDaEquipe = equipeAtual.getCidade();
			
			for (int rodada = 0; rodada < Parametros.NUMERO_MAXIMO_RODADAS; rodada++){
				
				//Buscar partida que a equipe est� jogando
				Partida partidaAtualDaEquipe = getPartidaDaEquipeNaRodada(equipeAtual, calendario.getCalendario()[rodada]);
				
				if (partidaAtualDaEquipe == null) //Ent�o n�o jogou na rodada
					continue;
				
				//Se a partida for num local diferente de onde a equipe estava, calcular deslocamento. Se n�o, n�o fazer nada
				if (partidaAtualDaEquipe.getMandante().getCidade().getId() != cidadeAtualDaEquipe.getId()){
					distanciaEquipe += matriz.getDistanciaEntreCidades(cidadeAtualDaEquipe, partidaAtualDaEquipe.getMandante().getCidade())/1000;
					cidadeAtualDaEquipe = partidaAtualDaEquipe.getMandante().getCidade();
				}
										
			}
			
			distanciaTotal += distanciaEquipe;
			
		}
		
		return distanciaTotal;
		
	}
	
	/**
	 * Retorna a partida da equipe da rodada
	 * Retorna null se a equipe n�o jogou na rodada
	 * @param equipe
	 * @param rodada
	 * @return
	 */
	private static Partida getPartidaDaEquipeNaRodada(Equipe equipe, Partida[] partidas){
		
		for (int i = 0; i < partidas.length; i++){
			if (partidas[i] != null && (partidas[i].getAdversario().getId() == equipe.getId() ||
					partidas[i].getMandante().getId() == equipe.getId()))
				return partidas[i];
		}
		
		return null;
	}
	
}
