package nba;

import java.util.ArrayList;
import java.util.Collection;

import utils.CalendarioUtilsNBA;
import utils.RoletaCalendario;

import dadosIniciais.Parametros;
import dominio.Calendario;

public class AlgoritmoGenetico {

	/**
	 * Obtem o melhor calend�rio utilizando o algoritmo gen�tico
	 * @return
	 */
	public static Calendario obterMelhorCalendario(){
		
		Calendario melhor = null;
		
		//Iniciar a popula��o
		Collection<Calendario> populacao = new ArrayList<Calendario>();
		for (int i = 0; i < Parametros.POPULACAO; i++){
			Calendario cal = CalendarioUtilsNBA.getCalendarioAleatorio();
			populacao.add(cal);
			System.out.println("Fitness inicial: " + Fitness.calcularFitness(cal));
		}
		
		//Repeti��o principal do la�o
		for (int i = 0; i < Parametros.NUMERO_LACOS_ALGORITMO_GENETICO; i++){
			
			//Calcular melhor calendario
			for (Calendario cal : populacao){
				if (cal.getFitness() == 0)
					cal.setFitness(Fitness.calcularFitness(cal));
				if (melhor == null || melhor.getFitness() > cal.getFitness())
					melhor = cal;
			}
			
			Collection<Calendario> populacao2 = new ArrayList<Calendario>(); 
			
			//Gerar descendentes e modifica-los
			for (int c = 0; i < Parametros.POPULACAO/2; i++){
				
				Calendario pai1 = SelecaoTorneio.selecionarPorTorneio(populacao, null);
				Calendario pai2 = SelecaoTorneio.selecionarPorTorneio(populacao, pai1);
				
				Calendario filho1 = Crossover.crossover1(pai1, pai2);
				Calendario filho2 = Crossover.crossover2(pai1, pai2);
				
				Mutacao.mutacao(filho1);
				Mutacao.mutacao(filho2);
				
				populacao2.add(filho1);
				populacao2.add(filho2);
				
			}
			
			populacao = populacao2;
			
		}
		
		return melhor;
		
	}
	
}
