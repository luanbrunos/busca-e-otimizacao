package nba;

import utils.ArrayUtils;
import dadosIniciais.Parametros;
import dominio.Calendario;
import dominio.Partida;

public class Crossover {

	/**
	 * Cruza os calendario1 e calendario2 e retorna o 1� filho
	 * O calend�rio � divido em 2 blocos: um at� a rodada 30 e outro a partir da rodada 31
	 * Os filhos s�o gerados a patir dos cruzamento dos blocos dos calend�rios pais
	 * @param calendario1
	 * @param calendario2
	 * @param filho1
	 * @param filho2
	 */
	public static Calendario crossover1(Calendario calendario1, Calendario calendario2){
		
		Partida[][] matrizFilho1 = new Partida[Parametros.NUMERO_MAXIMO_RODADAS][15];
		
		for (int i = 0; i < Parametros.NUMERO_MAXIMO_RODADAS; i++){
			
			if (i <= 29){
				matrizFilho1[i] = ArrayUtils.copiaPartidas(calendario1.getCalendario()[i]);
			} else {
				matrizFilho1[i] = ArrayUtils.copiaPartidas(calendario2.getCalendario()[i]);
			}
			
		}
		
		Calendario filho1 = new Calendario();
		filho1.setCalendario(matrizFilho1);

		return filho1;
		
	}
	
	/**
	 * Cruza os calendario1 e calendario2 e retorna o 2� filho
	 * O calend�rio � divido em 2 blocos: um at� a rodada 30 e outro a partir da rodada 31
	 * Os filhos s�o gerados a patir dos cruzamento dos blocos dos calend�rios pais
	 * @param calendario1
	 * @param calendario2
	 * @param filho1
	 * @param filho2
	 */
	public static Calendario crossover2(Calendario calendario1, Calendario calendario2){
		
		Partida[][] matrizFilho2 = new Partida[Parametros.NUMERO_MAXIMO_RODADAS][15];
		
		for (int i = 0; i < Parametros.NUMERO_MAXIMO_RODADAS; i++){
			
			if (i <= 29){
				matrizFilho2[i] = ArrayUtils.copiaPartidas(calendario2.getCalendario()[i]);
			} else {
				matrizFilho2[i] = ArrayUtils.copiaPartidas(calendario1.getCalendario()[i]);
			}
			
		}
		
		Calendario filho2 = new Calendario();
		filho2.setCalendario(matrizFilho2);
		
		return filho2;
	}
	
}
