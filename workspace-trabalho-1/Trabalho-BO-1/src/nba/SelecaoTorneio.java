package nba;

import java.util.ArrayList;
import java.util.Collection;

import utils.RoletaCalendario;
import utils.RoletaEquipe;

import dadosIniciais.Parametros;
import dominio.Calendario;

public class SelecaoTorneio {

	/**
	 * Seleciona um calendário dentre os calendários no método do torneio. Este tem que ser diferente de diferenteDe
	 * @param calendarios
	 * @param diferenteDe
	 * @return
	 */
	public static Calendario selecionarPorTorneio(Collection<Calendario> calendarios, Calendario diferenteDe){
		
		RoletaCalendario roleta = new RoletaCalendario(calendarios);
		
		//Seleciona n calendarios aleatórios
		Collection<Calendario> calendariosAleatorios = new ArrayList<Calendario>();
		for (int i = 0; i < Parametros.NUMERO_PAIS_TORNEIO; i++){
			Calendario c = roleta.getNextEquipe();
			if (diferenteDe != null && diferenteDe == c){
				i--;
				continue;
			}
			calendariosAleatorios.add(roleta.getNextEquipe());
		}
		
		//Dentre os calendários sorteados, pegar o melhor
		Calendario melhor = null;
		for (Calendario c : calendariosAleatorios){
			if (melhor == null || melhor.getFitness() > c.getFitness())
				melhor = c;
		}
		
		return melhor;
		
	}
	
}
