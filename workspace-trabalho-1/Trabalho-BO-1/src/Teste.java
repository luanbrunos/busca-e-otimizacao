import genetico.AlgoritmoGenetico;
import genetico.Fitness;
import hillClimbing.AlgoritmoHillClimbing;
import hillClimbing.AlgoritmoSteepestAscentHillClimbing;
import utils.CalendarioUtilsBRA;
import dadosIniciais.Parametros;
import dominio.Calendario;


public class Teste {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Calendario calendarioAtual = CalendarioUtilsBRA.getCalendarioAtualBrasileirao();
		calendarioAtual.setFitness(Fitness.calcularFitness(calendarioAtual));
		System.out.println("Fitness atual: "+calendarioAtual.getFitness());
		
		
		//Calendario melhorCalendario = AlgoritmoGenetico.obterMelhorCalendario();
		//Calendario melhorCalendario = AlgoritmoHillClimbing.obterMelhorCalendario();
		Calendario melhorCalendario = AlgoritmoSteepestAscentHillClimbing.obterMelhorCalendario();

		System.out.println("Melhor");
		//System.out.println(melhorCalendario);

	}

}
