package utils;

import java.util.Collection;
import java.util.Random;

import dadosIniciais.Dados;
import dominio.Calendario;
import dominio.Equipe;
import dominio.Partida;

public class CalendarioUtils {

	/**
	 * Retorna uma c�pia do calend�rio
	 * @param calendario
	 * @return
	 */
	public static Calendario copia(Calendario calendario){
		
		Calendario copia = new Calendario();
		
		for (int i = 0; i < calendario.getCalendario().length; i++)
			for (int j = 0; i < calendario.getCalendario()[i].length; j++){
				Partida p = new Partida(calendario.getCalendario()[i][j].getMandante(), 
										calendario.getCalendario()[i][j].getAdversario());
				copia.getCalendario()[i][j] = p;
			}
		
		return copia;
	}

	/**
	 * Retorna um calend�rio aleat�rio
	 * @return
	 */
	public static Calendario getCalendarioAleatorio(){
		
		Calendario aleatorio = new Calendario();
		
		/**
		 * Preencher calend�rio primeiramente com jogos de equipes de confer�ncias diferentes
		 */
		
		Equipe[] equipesLeste = ArrayUtils.toArrayEquipe(Dados.getEquipesConferenciaLeste());
		Equipe[] equipesOeste = ArrayUtils.toArrayEquipe(Dados.getEquipesConferenciaOeste());
 		
		RoletaEquipe roletaLeste = new RoletaEquipe(equipesLeste);
		
		int npartida = 0;
		
		while(roletaLeste.hasMoreEquipes()){
		
			Equipe eLeste = roletaLeste.getNextEquipe();
			
			for (int rodada = 0; rodada < equipesOeste.length*2; rodada+=2){
				
				Equipe[] equipesOesteQueAindaNaoJogaramNaRodada = getEquipesQueAindaNaoJogaram(equipesOeste, aleatorio.getCalendario()[rodada]);
				int numeroDeEquipes = equipesOesteQueAindaNaoJogaramNaRodada.length;
				Equipe eOeste = equipesOesteQueAindaNaoJogaramNaRodada[RandomUtils.getNumeroPositivoAleatorioAte(numeroDeEquipes-1)];
				
				Partida p1 = new Partida(eLeste, eOeste);
				Partida p2 = new Partida(eOeste, eLeste);
				
				aleatorio.getCalendario()[rodada][npartida] = p1;
				aleatorio.getCalendario()[rodada+1][npartida] = p2;				
				
			}	
			npartida++;
		}	
		
		/**
		 * Depois, preencher calend�rio com jogos contra equipes da mesma divis�o
		 */
		adicionarJogosContraPropriaDivisao(aleatorio, 30, ArrayUtils.toArrayEquipe(Dados.getEquipesAtlantico()), 0, 1);
		adicionarJogosContraPropriaDivisao(aleatorio, 30, ArrayUtils.toArrayEquipe(Dados.getEquipesCentral()), 2, 3);
		adicionarJogosContraPropriaDivisao(aleatorio, 30, ArrayUtils.toArrayEquipe(Dados.getEquipesSudeste()), 4, 5);
		adicionarJogosContraPropriaDivisao(aleatorio, 30, ArrayUtils.toArrayEquipe(Dados.getEquipesNoroeste()), 6, 7);
		adicionarJogosContraPropriaDivisao(aleatorio, 30, ArrayUtils.toArrayEquipe(Dados.getEquipesPacifico()), 8, 9);
		adicionarJogosContraPropriaDivisao(aleatorio, 30, ArrayUtils.toArrayEquipe(Dados.getEquipesSudoeste()), 10, 11);
		
		/**
		 * Por fim, preencher com jogos entre a mesma confer�ncia
		 */
		
		return aleatorio;
	}
	
	/**
	 * Retorna as equipes que est�o presentes em equipes, mas n�o est�o em partidas
	 * @param equipes
	 * @param partidas
	 * @return
	 */
	public static Equipe[] getEquipesQueAindaNaoJogaram(Equipe[] equipes, Partida[] partidas){
		
		Collection<Equipe> equipesCol = ArrayUtils.toCollectionEquipe(equipes);
		for (int i = 0; i < equipes.length; i++)
			for (int j = 0; j < partidas.length; j++){
				if (partidas[j] != null &&
						(partidas[j].getMandante() == equipes[i] || partidas[j].getAdversario() == equipes[i]))
					equipesCol.remove(equipes[i]);
			}
		
		return ArrayUtils.toArrayEquipe(equipesCol);
	}
	
	/**
	 * Adiciona ao calendario, a partir da rodada inicial, jogos das equipes nas posi��es de partida nPartida1 e nPartida2
	 * @param calendario
	 * @param rodadaInicial
	 * @param equipesDaDivisao
	 * @param nPartida1
	 * @param nPartida2
	 */
	private static void adicionarJogosContraPropriaDivisao(Calendario calendario, int rodadaInicial, Equipe[] equipesDaDivisao, int nPartida1, int nPartida2){
		
		RoletaEquipe roleta = new RoletaEquipe(equipesDaDivisao);
		Equipe[] equipeMisturado = new Equipe[equipesDaDivisao.length];
		equipeMisturado[0] = roleta.getNextEquipe();
		equipeMisturado[1] = roleta.getNextEquipe();
		equipeMisturado[2] = roleta.getNextEquipe();
		equipeMisturado[3] = roleta.getNextEquipe();
		equipeMisturado[4] = roleta.getNextEquipe();

		calendario.getCalendario()[rodadaInicial][nPartida1] = new Partida(equipeMisturado[1],equipeMisturado[2]);
		calendario.getCalendario()[rodadaInicial++][nPartida2] = new Partida(equipeMisturado[3],equipeMisturado[4]);
		calendario.getCalendario()[rodadaInicial][nPartida1] = new Partida(equipeMisturado[1],equipeMisturado[2]);
		calendario.getCalendario()[rodadaInicial++][nPartida2] = new Partida(equipeMisturado[3],equipeMisturado[4]);
		
		calendario.getCalendario()[rodadaInicial][nPartida1] = new Partida(equipeMisturado[2],equipeMisturado[1]);
		calendario.getCalendario()[rodadaInicial++][nPartida2] = new Partida(equipeMisturado[4],equipeMisturado[3]);
		calendario.getCalendario()[rodadaInicial][nPartida1] = new Partida(equipeMisturado[2],equipeMisturado[1]);
		calendario.getCalendario()[rodadaInicial++][nPartida2] = new Partida(equipeMisturado[4],equipeMisturado[3]);


			
		
	}
	
}
