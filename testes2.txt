Fitness atual: 646707
Fitness inicial: 648140
Fitness inicial: 645538
Fitness inicial: 652754
Fitness inicial: 647965
Fitness inicial: 646349
Fitness inicial: 664108
Ciclo: 1
Fitness do melhor: 639850
Tempo de execução: 0s

Ciclo: 10
Fitness do melhor: 621547
Tempo de execução: 0s

Ciclo: 100
Fitness do melhor: 555207
Tempo de execução: 0s

Ciclo: 1000
Fitness do melhor: 527253
Tempo de execução: 0s

Ciclo: 10000
Fitness do melhor: 519934
Tempo de execução: 6s

Ciclo: 100000
Fitness do melhor: 516963
Tempo de execução: 66s

Ciclo: 1000000
Fitness do melhor: 509173
Tempo de execução: 664s

Ciclo: 2000000
Fitness do melhor: 506276
Tempo de execução: 1330s

Ciclo: 3000000
Fitness do melhor: 505464
Tempo de execução: 2058s

Ciclo: 4000000
Fitness do melhor: 505152
Tempo de execução: 2886s

Ciclo: 5000000
Fitness do melhor: 502506
Tempo de execução: 3540s

Ciclo: 6000000
Fitness do melhor: 502416
Tempo de execução: 4194s

Ciclo: 7000000
Fitness do melhor: 502416
Tempo de execução: 4842s

Ciclo: 8000000
Fitness do melhor: 502416
Tempo de execução: 5498s

Ciclo: 9000000
Fitness do melhor: 502416
Tempo de execução: 6167s

Ciclo: 9999999
Fitness do melhor: 502416
Tempo de execução: 6830s

Melhor
1ª Rodada
Santa Cruz X Grêmio
Palmeiras X São Paulo
Corinthians X Atlético/PR
Botafogo X Figueirense
Cruzeiro X Ponte Preta
Atlético/MG X Santos
Sport X Internacional
Flamengo X Coritiba
Vitória X Chapecoense
América/MG X Fluminense

2ª Rodada
Vitória X Grêmio
Flamengo X São Paulo
Atlético/MG X Atlético/PR
Sport X Chapecoense
Santa Cruz X Internacional
América/MG X Ponte Preta
Palmeiras X Santos
Botafogo X Coritiba
Corinthians X Figueirense
Cruzeiro X Fluminense

3ª Rodada
Coritiba X Cruzeiro
Ponte Preta X Sport
Internacional X Palmeiras
São Paulo X Atlético/MG
Grêmio X América/MG
Figueirense X Flamengo
Santos X Vitória
Fluminense X Corinthians
Atlético/PR X Botafogo
Chapecoense X Santa Cruz

4ª Rodada
Internacional X Botafogo
Grêmio X Palmeiras
Atlético/PR X Sport
Coritiba X Corinthians
Chapecoense X Flamengo
Figueirense X América/MG
Ponte Preta X Santa Cruz
Santos X Cruzeiro
São Paulo X Vitória
Fluminense X Atlético/MG

5ª Rodada
Corinthians X Chapecoense
Cruzeiro X Grêmio
América/MG X São Paulo
Atlético/MG X Internacional
Santa Cruz X Santos
Sport X Figueirense
Flamengo X Fluminense
Palmeiras X Coritiba
Botafogo X Ponte Preta
Vitória X Atlético/PR

6ª Rodada
Coritiba X Atlético/PR
Vitória X Botafogo
Santos X Grêmio
Palmeiras X Corinthians
Sport X São Paulo
Ponte Preta X Fluminense
Cruzeiro X Flamengo
América/MG X Internacional
Atlético/MG X Chapecoense
Santa Cruz X Figueirense

7ª Rodada
Grêmio X Sport
Flamengo X Atlético/MG
Atlético/PR X Ponte Preta
São Paulo X Santa Cruz
Botafogo X Palmeiras
Figueirense X Vitória
Corinthians X Cruzeiro
Chapecoense X América/MG
Internacional X Santos
Fluminense X Coritiba

8ª Rodada
Grêmio X São Paulo
Cruzeiro X Sport
Botafogo X Atlético/MG
Flamengo X Santa Cruz
América/MG X Coritiba
Figueirense X Atlético/PR
Corinthians X Santos
Palmeiras X Vitória
Chapecoense X Ponte Preta
Fluminense X Internacional

9ª Rodada
Atlético/MG X Cruzeiro
Coritiba X Grêmio
Santa Cruz X Botafogo
São Paulo X Chapecoense
Sport X Palmeiras
Santos X Figueirense
Internacional X Corinthians
Vitória X América/MG
Ponte Preta X Flamengo
Atlético/PR X Fluminense

10ª Rodada
Atlético/PR X Grêmio
Coritiba X Internacional
Santa Cruz X Palmeiras
Cruzeiro X Botafogo
Sport X Corinthians
Ponte Preta X São Paulo
América/MG X Flamengo
Fluminense X Figueirense
Santos X Chapecoense
Atlético/MG X Vitória

11ª Rodada
Flamengo X Sport
Internacional X Cruzeiro
Figueirense X Coritiba
Corinthians X Ponte Preta
São Paulo X Santos
Vitória X Santa Cruz
Grêmio X Fluminense
Botafogo X América/MG
Palmeiras X Atlético/MG
Chapecoense X Atlético/PR

12ª Rodada
Santa Cruz X Sport
Ponte Preta X Atlético/MG
Flamengo X Corinthians
São Paulo X Atlético/PR
Internacional X Figueirense
Vitória X Fluminense
Palmeiras X América/MG
Grêmio X Botafogo
Coritiba X Santos
Chapecoense X Cruzeiro

13ª Rodada
Figueirense X Chapecoense
Corinthians X Vitória
Santos X Ponte Preta
Cruzeiro X Palmeiras
América/MG X Santa Cruz
Fluminense X São Paulo
Atlético/MG X Grêmio
Atlético/PR X Internacional
Botafogo X Flamengo
Sport X Coritiba

14ª Rodada
Figueirense X Ponte Preta
Corinthians X América/MG
Santos X Botafogo
Cruzeiro X São Paulo
Internacional X Grêmio
Fluminense X Palmeiras
Atlético/MG X Santa Cruz
Atlético/PR X Flamengo
Coritiba X Chapecoense
Sport X Vitória

15ª Rodada
Santa Cruz X Corinthians
Ponte Preta X Internacional
Flamengo X Santos
São Paulo X Coritiba
América/MG X Atlético/MG
Grêmio X Figueirense
Palmeiras X Atlético/PR
Botafogo X Sport
Chapecoense X Fluminense
Vitória X Cruzeiro

16ª Rodada
Santa Cruz X Cruzeiro
Ponte Preta X Coritiba
Santos X Atlético/PR
São Paulo X Internacional
América/MG X Sport
Vitória X Flamengo
Atlético/MG X Corinthians
Palmeiras X Figueirense
Botafogo X Fluminense
Chapecoense X Grêmio

17ª Rodada
Figueirense X São Paulo
Corinthians X Botafogo
Flamengo X Palmeiras
Internacional X Chapecoense
Fluminense X Santos
Grêmio X Ponte Preta
Atlético/PR X Santa Cruz
Coritiba X Vitória
Sport X Atlético/MG
Cruzeiro X América/MG

18ª Rodada
Figueirense X Atlético/MG
Ponte Preta X Palmeiras
Santos X América/MG
São Paulo X Corinthians
Internacional X Vitória
Fluminense X Sport
Grêmio X Flamengo
Atlético/PR X Cruzeiro
Coritiba X Santa Cruz
Chapecoense X Botafogo

19ª Rodada
Santa Cruz X Fluminense
Corinthians X Grêmio
Flamengo X Internacional
Cruzeiro X Figueirense
América/MG X Atlético/PR
Vitória X Ponte Preta
Atlético/MG X Coritiba
Palmeiras X Chapecoense
Botafogo X São Paulo
Sport X Santos

20ª Rodada
Grêmio X Santa Cruz
São Paulo X Palmeiras
Atlético/PR X Corinthians
Figueirense X Botafogo
Ponte Preta X Cruzeiro
Santos X Atlético/MG
Internacional X Sport
Coritiba X Flamengo
Chapecoense X Vitória
Fluminense X América/MG

21ª Rodada
Grêmio X Vitória
São Paulo X Flamengo
Atlético/PR X Atlético/MG
Chapecoense X Sport
Internacional X Santa Cruz
Ponte Preta X América/MG
Santos X Palmeiras
Coritiba X Botafogo
Figueirense X Corinthians
Fluminense X Cruzeiro

22ª Rodada
Cruzeiro X Coritiba
Sport X Ponte Preta
Palmeiras X Internacional
Atlético/MG X São Paulo
América/MG X Grêmio
Flamengo X Figueirense
Vitória X Santos
Corinthians X Fluminense
Botafogo X Atlético/PR
Santa Cruz X Chapecoense

23ª Rodada
Botafogo X Internacional
Palmeiras X Grêmio
Sport X Atlético/PR
Corinthians X Coritiba
Flamengo X Chapecoense
América/MG X Figueirense
Santa Cruz X Ponte Preta
Cruzeiro X Santos
Vitória X São Paulo
Atlético/MG X Fluminense

24ª Rodada
Chapecoense X Corinthians
Grêmio X Cruzeiro
São Paulo X América/MG
Internacional X Atlético/MG
Santos X Santa Cruz
Figueirense X Sport
Fluminense X Flamengo
Coritiba X Palmeiras
Ponte Preta X Botafogo
Atlético/PR X Vitória

25ª Rodada
Atlético/PR X Coritiba
Botafogo X Vitória
Grêmio X Santos
Corinthians X Palmeiras
São Paulo X Sport
Fluminense X Ponte Preta
Flamengo X Cruzeiro
Internacional X América/MG
Chapecoense X Atlético/MG
Figueirense X Santa Cruz

26ª Rodada
Sport X Grêmio
Atlético/MG X Flamengo
Ponte Preta X Atlético/PR
Santa Cruz X São Paulo
Palmeiras X Botafogo
Vitória X Figueirense
Cruzeiro X Corinthians
América/MG X Chapecoense
Santos X Internacional
Coritiba X Fluminense

27ª Rodada
São Paulo X Grêmio
Sport X Cruzeiro
Atlético/MG X Botafogo
Santa Cruz X Flamengo
Coritiba X América/MG
Atlético/PR X Figueirense
Santos X Corinthians
Vitória X Palmeiras
Ponte Preta X Chapecoense
Internacional X Fluminense

28ª Rodada
Cruzeiro X Atlético/MG
Grêmio X Coritiba
Botafogo X Santa Cruz
Chapecoense X São Paulo
Palmeiras X Sport
Figueirense X Santos
Corinthians X Internacional
América/MG X Vitória
Flamengo X Ponte Preta
Fluminense X Atlético/PR

29ª Rodada
Grêmio X Atlético/PR
Internacional X Coritiba
Palmeiras X Santa Cruz
Botafogo X Cruzeiro
Corinthians X Sport
São Paulo X Ponte Preta
Flamengo X América/MG
Figueirense X Fluminense
Chapecoense X Santos
Vitória X Atlético/MG

30ª Rodada
Sport X Flamengo
Cruzeiro X Internacional
Coritiba X Figueirense
Ponte Preta X Corinthians
Santos X São Paulo
Santa Cruz X Vitória
Fluminense X Grêmio
América/MG X Botafogo
Atlético/MG X Palmeiras
Atlético/PR X Chapecoense

31ª Rodada
Sport X Santa Cruz
Atlético/MG X Ponte Preta
Corinthians X Flamengo
Atlético/PR X São Paulo
Figueirense X Internacional
Fluminense X Vitória
América/MG X Palmeiras
Botafogo X Grêmio
Santos X Coritiba
Cruzeiro X Chapecoense

32ª Rodada
Chapecoense X Figueirense
Vitória X Corinthians
Ponte Preta X Santos
Palmeiras X Cruzeiro
Santa Cruz X América/MG
São Paulo X Fluminense
Grêmio X Atlético/MG
Internacional X Atlético/PR
Flamengo X Botafogo
Coritiba X Sport

33ª Rodada
Ponte Preta X Figueirense
América/MG X Corinthians
Botafogo X Santos
São Paulo X Cruzeiro
Grêmio X Internacional
Palmeiras X Fluminense
Santa Cruz X Atlético/MG
Flamengo X Atlético/PR
Chapecoense X Coritiba
Vitória X Sport

34ª Rodada
Corinthians X Santa Cruz
Internacional X Ponte Preta
Santos X Flamengo
Coritiba X São Paulo
Atlético/MG X América/MG
Figueirense X Grêmio
Atlético/PR X Palmeiras
Sport X Botafogo
Fluminense X Chapecoense
Cruzeiro X Vitória

35ª Rodada
Cruzeiro X Santa Cruz
Coritiba X Ponte Preta
Atlético/PR X Santos
Internacional X São Paulo
Sport X América/MG
Flamengo X Vitória
Corinthians X Atlético/MG
Figueirense X Palmeiras
Fluminense X Botafogo
Grêmio X Chapecoense

36ª Rodada
São Paulo X Figueirense
Botafogo X Corinthians
Palmeiras X Flamengo
Chapecoense X Internacional
Santos X Fluminense
Ponte Preta X Grêmio
Santa Cruz X Atlético/PR
Vitória X Coritiba
Atlético/MG X Sport
América/MG X Cruzeiro

37ª Rodada
Atlético/MG X Figueirense
Palmeiras X Ponte Preta
América/MG X Santos
Corinthians X São Paulo
Vitória X Internacional
Sport X Fluminense
Flamengo X Grêmio
Cruzeiro X Atlético/PR
Santa Cruz X Coritiba
Botafogo X Chapecoense

38ª Rodada
Fluminense X Santa Cruz
Grêmio X Corinthians
Internacional X Flamengo
Figueirense X Cruzeiro
Atlético/PR X América/MG
Ponte Preta X Vitória
Coritiba X Atlético/MG
Chapecoense X Palmeiras
São Paulo X Botafogo
Santos X Sport


